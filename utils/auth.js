const TokenKey = 'App-Token'

export function getToken() {
	// console.log(uni.getStorageSync(TokenKey))
	return uni.getStorageSync(TokenKey)
}
export function setToken(token) {
	return uni.setStorageSync(TokenKey, token)
}
export function removeToken() {
	return uni.removeStorageSync(TokenKey)
}
export function getUser() {
	return uni.getStorageSync("userObj")
}
export function setUser(user) {
	return uni.setStorageSync("userObj", user)
}
export function removeUser() {
	return uni.removeStorageSync("userObj")
}

export function getInfoId() {
	return uni.getStorageSync("infoId")
}
export function setInfoId(infoId) {
	return uni.setStorageSync("infoId", infoId)
}
export function removeInfoId() {
	return uni.removeStorageSync("infoId")
}
//roleKey

export function getInforoleKey() {
	return uni.getStorageSync("roleKey")
}
export function setInforoleKey(roleKey) {
	return uni.setStorageSync("roleKey", roleKey)
}
export function removeInforoleKey() {
	return uni.removeStorageSync("roleKey")
}