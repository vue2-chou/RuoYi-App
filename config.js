// 应用全局配置
module.exports = {
	// baseUrl: 'https://vue.ruoyi.vip/prod-api',
	// baseUrl: 'https://4h3571076r.zicp.fun',
	// baseUrl: 'http://61.172.177.150:8080',
	baseUrl: 'http://192.168.15.38:8080',
	// baseUrl: 'https://soft.cmjy.sh.cn:8080',
	// 应用信息
	appInfo: {
		// 应用名称
		name: "ganxun-app",
		// 应用版本
		version: "1.1.0",
		// 应用logo
		logo: "/static/login/login.png",
		// 官方网站
		site_url: "https://www.baidu.com",
		// 政策协议
		agreements: [{
				title: "隐私政策",
				url: "https://jz.vip/protocol.html"
			},
			{
				title: "用户服务协议",
				url: "https://jz.vip/protocol.html"
			}
		]
	}
}