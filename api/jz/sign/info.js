import request from '@/utils/request'

// 查询学生签到列表
export function listInfo(query) {
	return request({
		url: '/sign/info/list',
		method: 'get',
		params: query
	})
}

// 查询学生签到详细
export function getInfo(id) {
	return request({
		url: '/sign/info/' + id,
		method: 'get'
	})
}

// 新增学生签到
export function addInfo(data) {
	return request({
		url: '/sign/info',
		method: 'post',
		data: data
	})
}

// 修改学生签到
export function updateInfo(data) {
	return request({
		url: '/sign/info',
		method: 'put',
		data: data
	})
}

// 删除学生签到
export function delInfo(id) {
	return request({
		url: '/sign/info/' + id,
		method: 'delete'
	})
}

export function getSignInByTeacher(data) {
	return request({
		url: '/sign/info/get/list/by/teacher',
		method: 'get',
		data: data
	})
}

export function getQRCode(data) {
	return request({
		url: '/sign/info/QRcode/create',
		method: 'get',
		data: data
	})
}


export function getAllSign(data) {
	return request({
		url: '/get/student/sign/info',
		method: 'get',
		data: data
	})
}
export function getSignByStudentId() {
	return request({
		url: '/get/sign/info/by/studentId',
		method: 'get'
	})
}

export function getCourseInfoSignByStudentId(studentId) {
	return request({
		url: '/getCourseInfoSignByStudentId/' + studentId,
		method: 'get'
	})
}