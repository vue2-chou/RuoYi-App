import request from '@/utils/request'

export function getStudentList(data) {
	return request({
		url: '/student/info/list2',
		method: 'get',
		data: data
	})
}

export function sansQrCode(data) {
	return request({
		url: '/sign/info/sign/in',
		method: 'post',
		data: data
	})
}