import request from '@/utils/request'

// 查询报名信息列表
export function listInfo(query) {
	return request({
		url: '/apply/info/list',
		method: 'get',
		params: query
	})
}

// 查询报名信息详细
export function getInfo(id) {
	return request({
		url: '/apply/info/' + id,
		method: 'get'
	})
}

// 新增报名信息
export function addInfo(data) {
	return request({
		url: '/apply/info',
		method: 'post',
		data: data
	})
}

// 修改报名信息
export function updateInfo(data) {
	return request({
		url: '/apply/info',
		method: 'put',
		data: data
	})
}

// 删除报名信息
export function delInfo(id) {
	return request({
		url: '/apply/info/' + id,
		method: 'delete'
	})
}


export function getApplyListBySchoolId(schoolId, status) {
	return request({
		url: '/apply/info/getApplyListBySchoolId/' + schoolId + '/' + status,
		method: 'get',
	})
}

export function getApplyByuserId() {
	return request({
		url: '/get/apply/by/userId',
		method: 'get',
	})
}