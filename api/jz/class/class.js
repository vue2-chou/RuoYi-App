import request from '@/utils/request'

export function getClassInfoByTeacherId(data) {
	return request({
		'url': '/teacher/getClassInfoByTeacherId',
		'method': 'get',
	})
}
export function getStudentInfoByClassId(data) {
	return request({
		'url': '/teacher/getStudentInfoByClassId?classId=' + data,
		'method': 'get',
	})
}
export function addWorkInfo(data) {
	return request({
		'url': '/work/info/add',
		'method': 'post',
		'data': data
	})
}
export function getWorkInfoList(data) {
	return request({
		'url': '/work/info/list',
		'method': 'get',
		'data': data
	})
}
export function delWorkJob(data) {
	return request({
		'url': '/work/info/del?id=' + data,
		'method': 'post',
	})
}
export function editWorkJob(data) {
	return request({
		'url': '/work/info/edit',
		'method': 'put',
		'data': data
	})
}
export function addCourse(data) {
	return request({
		'url': '/course/info/addCourse',
		'method': 'get',
		'data': data
	})
}
export function getStuPaperList() {
	return request({
		'url': '/historyPaper/paper/getStuPaperList',
		'method': 'get',
	})
}
export function getStuPaperListByStuId(data) {
	return request({
		'url': '/historyPaper/paper/getStuPaperListByStuId',
		'method': 'get',
		'data': data
	})
}
//做罪业
///workStudent/info
export function doWork(data) {
	return request({
		'url': '/workStudent/info',
		'method': 'post',
		'data': data
	})
}
//workStudent/info

export function updateWorkInfo(data) {
	return request({
		url: '/workStudent/info',
		method: 'put',
		data: data
	})
}

export function getClassInfoAll() {
	return request({
		url: '/class/info/getClassInfoAll',
		method: 'get',
	})
}

///get/class 查看学生所在班级

export function getClassInfo2() {
	return request({
		url: '/get/class',
		method: 'get',
	})
}

///get/zs/by/classId 获取证书
export function getzs(data) {
	return request({
		url: '/get/zs/by/classId',
		method: 'get',
		
		data: data
	})
}