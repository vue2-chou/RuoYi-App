import request from '@/utils/request'

// 查询学校基本信息管理列表
export function listInfo(query) {
	return request({
		url: '/school/info/list',
		method: 'get',
		params: query
	})
}

// 查询学校基本信息管理详细
export function getInfo(id) {
	return request({
		url: '/school/info/' + id,
		method: 'get'
	})
}

// 新增学校基本信息管理
export function addInfo(data) {
	return request({
		url: '/school/info',
		method: 'post',
		data: data
	})
}

// 修改学校基本信息管理
export function updateInfo(data) {
	return request({
		url: '/school/info',
		method: 'put',
		data: data
	})
}

// 删除学校基本信息管理
export function delInfo(id) {
	return request({
		url: '/school/info/' + id,
		method: 'delete'
	})
}

export function getSchoolInfoByUserId(userId) {
	return request({
		url: '/school/info/getSchoolInfoByUserId/' + userId,
		method: 'get'
		// params: userId
	})
}

export function getStudentInfoListBySchoolId(schoolId) {
	return request({
		url: '/getStudentInfoListBySchoolId/' + schoolId,
		method: 'get'
	})
}