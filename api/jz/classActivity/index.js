import request from '@/utils/request'

// 查询班级活动列表
export function listInfo(query) {
  return request({
    url: '/classActivity/info/list',
    method: 'get',
    params: query
  })
}

// 查询班级活动详细
export function getInfo(id) {
  return request({
    url: '/classActivity/info/' + id,
    method: 'get'
  })
}

// 新增班级活动
export function addInfo(data) {
  return request({
    url: '/classActivity/info',
    method: 'post',
    data: data
  })
}

// 修改班级活动
export function updateInfo(data) {
  return request({
    url: '/classActivity/info',
    method: 'put',
    data: data
  })
}


// 删除
export function delteClassActivityInfo(data) {
  return request({
    url: '/classActivity/info/'+data,
    method: 'delete',
    // data: data
  })
}

// 删除班级活动
export function delInfo(id) {
  return request({
    url: '/classActivity/info/' + id,
    method: 'delete'
  })
}

export function getListByTeacherId(data) {
  return request({
    url: '/get/class/activity/list',
    method: 'get',
	params:data
  })
}

export function deleteByActivityId(id) {
  return request({
    url: '/delete/classActivity/by/'+id,
    method: 'post'
  })
}
export function sendSaveActivity(data) {
  return request({
    url: '/save/send/activity',
    method: 'post',
	data:data
  })
}

///teacher/getActiveByTeacherId

export function getActiveByTeacherId(data) {
  return request({
    url: '/teacher/getActiveByTeacherId',
    method: 'get',
	data:data
  })
}


