import request from '@/utils/request'

export function getAppQuestionaireInfo(data) {
	return request({
		'url': '/questionnaire/info/getAppQuestionaireInfo',
		'method': 'get',
		'data':data

	})
}
//调查问卷记录
export function addAppQuestionaireInfo(data) {
	return request({
		'url': '/questionnaireRecode/info',
		'method': 'post',
		'data':data

	})
}