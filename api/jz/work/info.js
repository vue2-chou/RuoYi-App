import request from '@/utils/request'

// 查询作业-学生列表
export function listInfo(query) {
	return request({
		url: '/workStudent/info/list',
		method: 'get',
		params: query
	})
}

// 查询作业-学生详细
export function getInfo(id) {
	return request({
		url: '/workStudent/info/' + id,
		method: 'get'
	})
}

// 新增作业-学生
export function addInfo(data) {
	return request({
		url: '/workStudent/info',
		method: 'post',
		data: data
	})
}

// 修改作业-学生
export function updateInfo(data) {
	return request({
		url: '/workStudent/info',
		method: 'put',
		data: data
	})
}

// 删除作业-学生
export function delInfo(id) {
	return request({
		url: '/workStudent/info/' + id,
		method: 'delete'
	})
}