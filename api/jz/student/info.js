import request from '@/utils/request'

export function getStudentList(data) {
  return request({
    url: '/student/info/list2',
    method: 'get',
	data:data
  })
}
export function getMyCourse(data) {
  return request({
    url: '/student/getMyCourse',
    method: 'get',
	data:data
  })
}
// student/getActiveByStudentId/

export function getActiveByStudentId(data) {
  return request({
    url: '/student/getActiveByStudentId?studentId='+data,
    method: 'get',
	// data:data
  })
}

///student/getWorkInfoByStudentId
export function getWorkInfoByStudentId(data) {
  return request({
    url: '/student/getWorkInfoByStudentId?studentId='+data,
    method: 'get',
	// data:data
  })
}

///All/getWorkInfoAndWorkStudentByStudentId

export function getWorkInfoAndWorkStudentByStudentId(data) {
  return request({
    url: '/All/getWorkInfoAndWorkStudentByStudentId?studentId='+data,
    method: 'get',
	// data:data
  })
}

///get/credit/{userId}

export function getCreditByuserId(data) {
  return request({
    url: '/get/credit/'+data,
    method: 'get',
	// data:data
  })
}
