import request from '@/utils/request'

export function getStuPaperListByStuId(data) {
    return request({
        'url': '/historyPaper/paper/getStuPaperListByStuId?studenId='+data,
        'method': 'get',
		// 'data': data
    })
}


export function edit(data) {
    return request({
        'url': '/historyPaper/paper/edit',
        'method': 'put',
		'data': data
    })
}
