import request from '@/utils/request'

export function getMyClassActivity(data) {
    return request({
        url: '/get/my/class/activity',
        method: 'get',
		params: data
    })
}