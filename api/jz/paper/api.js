import request from '@/utils/request'

export function getPaperInfoByuserId(data) {
	return request({
		'url': '/paper/info/getPaperInfoByuserId',
		'method': 'get',

	})
}


export function getinfoListBypId(data) {
	return request({
		'url': '/question/info/getinfoListBypId?paperId=' + data,
		'method': 'get',

	})
}
export function submitPaper(data) {
	return request({
		'url': '/historyPaper/paper/add',
		'method': 'post',
		'data': data
	})
}