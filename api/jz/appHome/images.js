import request from '@/utils/request'

// 查询app首页图片列表
export function listImages(query) {
	return request({
		url: '/appHome/images/list',
		method: 'get',
		params: query
	})
}

// 查询app首页图片详细
export function getImages(id) {
	return request({
		url: '/appHome/images/' + id,
		method: 'get'
	})
}

// 新增app首页图片
export function addImages(data) {
	return request({
		url: '/appHome/images',
		method: 'post',
		data: data
	})
}

// 修改app首页图片
export function updateImages(data) {
	return request({
		url: '/appHome/images',
		method: 'put',
		data: data
	})
}

// 删除app首页图片
export function delImages(id) {
	return request({
		url: '/appHome/images/' + id,
		method: 'delete'
	})
}