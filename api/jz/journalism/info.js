import request from '@/utils/request'

// 查询app首页新闻列表
export function listInfo(query) {
	return request({
		url: '/journalism/info/list',
		method: 'get',
		params: query
	})
}

// 查询app首页新闻详细
export function getInfo(id) {
	return request({
		url: '/journalism/info/' + id,
		method: 'get'
	})
}

// 新增app首页新闻
export function addInfo(data) {
	return request({
		url: '/journalism/info',
		method: 'post',
		data: data
	})
}

// 修改app首页新闻
export function updateInfo(data) {
	return request({
		url: '/journalism/info',
		method: 'put',
		data: data
	})
}

// 删除app首页新闻
export function delInfo(id) {
	return request({
		url: '/journalism/info/' + id,
		method: 'delete'
	})
}