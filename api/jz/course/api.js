import request from '@/utils/request'

export function addCourse(data) {
    return request({
        'url': '/course/info/addCourse',
        'method': 'post',
		'data': data
    })
}


export function getTeacherCourseListByClassId(data) {
    return request({
        'url': '/teacher/getTeacherCourseListByClassId',
        'method': 'post',
		'data': data
    })
}

export function delByid(data) {
    return request({
        'url': '/course/info/delByid?id='+data,
        'method': 'post',
		// 'data': data
    })
}

export function getClassInfoByTeacherId() {
    return request({
        'url': '/teacher/getClassInfoByTeacherId',
        'method': 'get',
		// 'data': data
    })
}