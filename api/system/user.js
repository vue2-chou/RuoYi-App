import upload from '@/utils/upload'
import request from '@/utils/request'

// 用户密码重置
export function updateUserPwd(oldPassword, newPassword) {
	const data = {
		oldPassword,
		newPassword
	}
	return request({
		url: '/system/user/profile/updatePwd',
		method: 'put',
		params: data
	})
}

// 查询用户个人信息
export function getUserProfile() {
	return request({
		url: '/system/user/profile',
		method: 'get'
	})
}

// 修改用户个人信息
export function updateUserProfile(data) {
	return request({
		url: '/system/user/profile',
		method: 'put',
		data: data
	})
}

// 新增学生信息
export function addStuInfo(data) {
	return request({
		url: '/student/info',
		method: 'post',
		data: data
	})
}


// 查询学生信息详细
export function getStuInfo(id) {
	return request({
		url: '/student/info/' + id,
		method: 'get'
	})
}

//查询老师个人资料
export function getteacherInfoByteaId(id) {
	return request({
		url: '/teacher/info/' + id,
		method: 'get'
	})
}

// 修改学生信息
export function updateStuInfo(data) {
	return request({
		url: '/student/info',
		method: 'put',
		data: data
	})
}

// 用户头像上传
export function uploadAvatar(data) {
	return upload({
		url: '/system/user/profile/avatar',
		name: data.name,
		filePath: data.filePath
	})
}

// 读取学生学历
export function getSysDictDataByDictType(dictType) {
	return request({
		url: '/getSysDictDataByDictType/' + dictType,
		method: 'get',
	})
}